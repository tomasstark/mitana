import React from 'react';
import ReactDOM from 'react-dom';
import { Router, IndexRoute, Route, Link, browserHistory } from 'react-router';
import DocumentMeta from 'react-document-meta';

let Header = require('./components/bootstrap/header');
let Main = require('./components/main');
let Social = require('./components/social');
let Lang = require('./components/lang');

class App extends React.Component {
    render() {
        let langProp = this.props.route.lang;

        if (!langProp) {
            langProp = navigator.language.substring(0, 2);
        }

        const meta = {
            title: Lang.getTranslation(langProp, ["main", "h1"]),
            description: Lang.getTranslation(langProp, ["og", "description"]),
            meta: {
                property: {
                    'og:title': Lang.getTranslation(langProp, ["main", "h1"]),
                    'og:description': Lang.getTranslation(langProp, ["og", "description"]),
                    'og:type': 'article',
                    'og:image': 'http://'+document.domain+'/img/civava-og.jpg'
                }
            }
        }

        return (
            <main className="container">
                <DocumentMeta {...meta} />

                <div className="grid">
                    <div className="col-3">
                        <div className="content">
                            <div className="content__first">
                                <Header lang={langProp} />
                                <Main lang={langProp} />
                            </div>
                            <div className="social">
                                <Social lang={langProp} />
                            </div>
                        </div>
                    </div>
                    <div className="col-2 colored-cols">
                        <div className="grid">
                            <div className="col-1 colored-col colored-col--yellow">
                                <div className="chihuahua"></div>
                            </div>
                            <div className="col-1 colored-col--red"></div>
                        </div>
                    </div>
                </div>
            </main>
        )
    }
}

const Container = (props) => <div>
    {props.children}
</div>

ReactDOM.render((
    <Router history={browserHistory}>
        <Route path="/" component={Container}>
            <IndexRoute component={App} />
            <Route path="sk" component={App} lang="sk" />
        </Route>
    </Router>
), document.getElementById('app'));
