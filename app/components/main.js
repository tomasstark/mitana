import React from 'react';
import ReactDOM from 'react-dom';

let Lang = require('./lang');

class Main extends React.Component {
    render() {
        return (
            <div>
                <section dangerouslySetInnerHTML={{__html: Lang.getTranslation(this.props.lang, ["main", "intro"])}}></section>
            </div>
        )
    }
}

module.exports = Main;
