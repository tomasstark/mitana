import React from 'react';
import ReactDOM from 'react-dom';

let Lang = require('../lang');

class Header extends React.Component {
    render() {
        return (
            <header className="header">
                <h1 className="header__h1">{Lang.getTranslation(this.props.lang, ['main', 'h1'])}</h1>
            </header>
        )
    }
}

module.exports = Header;
