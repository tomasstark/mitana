import React from 'react';

let Lang = require('./lang');

class Social extends React.Component {
    render() {
        return (
            <ul className="social__list">
                <li className="social__list-item"><a href="https://www.behance.net/mitana" title={Lang.getTranslation(this.props.lang, ['social', 'portfolio'])} target="_blank">{Lang.getTranslation(this.props.lang, ['social', 'portfolio'])}</a></li>
                <li className="social__list-item"><a href="https://dribbble.com/tomasmitana" title={Lang.getTranslation(this.props.lang, ['social', 'dribbble'])} target="_blank">{Lang.getTranslation(this.props.lang, ['social', 'dribbble'])}</a></li>
                <li className="social__list-item"><a href="https://twitter.com/tomasmitana" title={Lang.getTranslation(this.props.lang, ['social', 'twitter'])} target="_blank">{Lang.getTranslation(this.props.lang, ['social', 'twitter'])}</a></li>
                <li className="social__list-item"><a href="mailto:hi@tomasmitana.com" title={Lang.getTranslation(this.props.lang, ['social', 'email'])} target="_blank">{Lang.getTranslation(this.props.lang, ['social', 'email'])}</a></li>
            </ul>
        )
    }
}

module.exports = Social;
