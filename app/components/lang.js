import React from 'react';

class Lang extends React.Component {
    static getTranslation(lang, path) {
        if (typeof lang !== 'undefined') {
            var language;

            switch (lang) {
                case 'en':
                    language = require('../lang/en.json');
                    break;

                case 'sk':
                    language = require('../lang/sk.json');
                    break;

                default:
                    language = require('../lang/en.json');
                    break;
            }

            return language[path[0]][path[1]];
        }
    }

    render() {
    }
}

module.exports = Lang;
